<?php

class DATABASE {

private $host = DB_HOST;

private $user = DB_USER;

private $pass = DB_PASS;

private $dbname = DB_NAME;

private $dbh; 

private $stmt;

private $error;

public function __construct(){

 $dsn = 'mysql:host='.$this->host.';port=3306;dbname='.$this->dbname;

 $options = [


 PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",

 PDO::ATTR_PERSISTENT => true, 

 PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION

 ];

 try {

 $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);

 } catch (PDOException $e) {

 $this->error = $e->getMessage();

 echo $this->error;

 }

}

public function query (string $SQL){

    $this->stmt = $this->dbh->prepare($SQL);
   
}

public function bind($param, $value, $type=null) {

    if (is_null($type)) {
   
    switch(true) {
   
    case is_int($value):
   
    $type = PDO::PARAM_INT;
   
    break;
   
    case is_bool($value):
   
    $type = PDO::PARAM_BOOL;
   
    break;
   
    case is_null($value):
   
    $type = PDO::PARAM_NULL;
   
    break;
   
    default:
   
    $type = PDO::PARAM_STR;
   
    }
   
    }
   
    $this->stmt->bindValue($param, $value, $type);
   
}

public function execute() {

    return $this->stmt->execute();
   
}

public function resultSet($model) {

    $this->execute();
   //Usamos PROPS_LATE para que haga el set de las propiedades después de llamar al construtor
    return $this->stmt->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $model);
   
}

}
?>