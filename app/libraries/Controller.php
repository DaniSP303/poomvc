<?php

class Controller{


    public function model ($modelo) {
        require "../app/models/".$modelo.'.php';
        return new $modelo;
        }

    public function view ($vista, $datos = []) {
        if (file_exists('../app/views/' . $vista . '.php')) {
            require "../app/views/".$vista .'.php';

            }else
            {
                print "Error cargando la vista" and die();         // Si el fichero existe lo carga, en caso contrario informa del error y muere
            }

        }
   
   }

?>