<?php


class Posts extends Controller{



    public function __construct() {
   
        $this->postModel =  $this->model("Post");

    }

    public function index()
    {
        $posts = $this->postModel->getPosts();
        
        $datos = [
        'titulo'=>'Bienvenido',
        'posts'=>$posts
        ];
        return $this->view('posts/index', $datos);
    }

}

?>